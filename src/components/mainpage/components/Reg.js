import React,{ Component } from 'react';
import './css/Reg.css';
// import leftBgImg from './img/left.jpg';

class Reg extends Component {
  render(){
    return (
      <div className='reg-wrap-bg'>
        <div className='reg-text'>
          <h1>ENTER</h1>
        </div>
        <div className='reg-bg-pic'></div>

        <div className='reg-blur-layer'></div>

        <div className='reg-wrap'>

        </div>

      </div>
    );
  }
}
export default Reg;
