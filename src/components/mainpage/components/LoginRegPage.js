import React, { Component } from 'react';
import LoginRegHolder from './LoginRegHolder';

class LoginRegPage extends Component {


  render(){
    return (
      <div className='login-reg-page'>
        <LoginRegHolder />
      </div>
    );
  }

}

export default LoginRegPage;
