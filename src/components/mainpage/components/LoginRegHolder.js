import React, {Component} from 'react';
import './css/LoginRegHolder.css';

import Login from './Login';
import Reg from './Reg';

class LoginRegHolder extends Component {

  render() {
    return (
      <div className='login-reg-fullpage'>
        <div className='login-reg-fullpage-header'>
          <h1>social !?</h1>
        </div>

      <div className='login-reg-holder'>
        <div className='login-left'>
          <Login />
        </div>
        <div className='reg-right'>
          <Reg />
        </div>
      </div>
      <div className='login-reg-fullpage-footer'>
        <h1>2018 <span> @kano</span></h1>
      </div>
    </div>
  );
  }
}
export default LoginRegHolder;
