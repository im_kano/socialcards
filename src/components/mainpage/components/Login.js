import React,{ Component } from 'react';
import './css/Login.css';
// import leftBgImg from './img/left.jpg';

import LoginForm from './LoginForm';

class Login extends Component {
  render(){
    return (
      <div className='login-wrap-bg'>
        <div className='login-text'>
          <h1>LOGIN</h1>
        </div>
        <div className='bg-pic'></div>
        <div className='blur-layer'></div>

        <div className='login-wrap'>
          <h1>Enter your data to login</h1>

          <LoginForm />
          
        </div>

      </div>
    );
  }
}
export default Login;
