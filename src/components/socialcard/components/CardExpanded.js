import './css/CardExpanded.css';
import React,{Component} from 'react';
import logo from './img/logo.png';
import logo2 from './img/logo2.png';
import testImage from './img/3.jpg';


class CardExpanded extends Component {

  handleCardExpand = (id,flag) => {
    this.props.handleExpandClose(id,flag);
  };

  render(){
    const color = {
      backgroundColor: this.props.article.color,
      color: this.props.article.textcolor,
    };
    const articleObj = this.props.article;

    return(
      <div className='card-expanded' style={color}>

        <div className='card-expanded-header'>
          <div className='card-expanded-header-logo'>
            <img src={articleObj.logo === 'logo' ? logo : logo2} alt='logo'/>
          </div>
          <div className='card-expanded-header-title'>
            <div className='subject'><h1>{articleObj.title}</h1><span> {articleObj.date} </span></div>
            <div className='theme'><h1>{articleObj.articletitle}</h1></div>
            <div className='author'><h1>{articleObj.author}</h1></div>
          </div>
          <div className='card-expanded-header-arrow'>
            <div><i
              className="fa fa-arrow-up"
              onClick={() => this.handleCardExpand(articleObj.id, articleObj.isexpanded)}
              ></i></div>
          </div>
        </div>

        <div className='card-expanded-content'>
          <div className='line'></div>
          <div className='card-expanded-content-text'>
            <p>{articleObj.text}</p>
          </div>
          <div className='card-expanded-content-image'>
            <img
              src={testImage}
              alt='testImage'

             />
          </div>
          <div className='card-expanded-content-photoby'>
            <h1>by {articleObj.picauthor}</h1>
          </div>
        </div>
        <div className='bottom-delimiter'></div>


        <div className='card-expanded-bottom'>
          <div className='card-expanded-bottom-repost'>
            <div><i className="fa fa-share"></i><span>{articleObj.stats[0]}</span></div>
          </div>
          <div className='card-expanded-bottom-share'>
            <div><i className="fa fa-retweet"></i><span>{articleObj.stats[1]}</span></div>
          </div>
          <div className='card-expanded-bottom-comment'>
            <div><i className="fa fa-comment"></i><span>{articleObj.stats[2]}</span></div>
          </div>
          <div className='card-expanded-bottom-like'>
            <div><i className="fa fa-heart"></i><span>{articleObj.stats[3]}</span></div>
          </div>
        </div>


      </div>
    );
  }
}
export default CardExpanded;
