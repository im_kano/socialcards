import React, {Component} from 'react';
import CardMini from './CardMini';
import uuid from 'uuid';



import './css/CardExpandHolder.css';

import CardExpanded from './CardExpanded';

class CardExpandHolder extends Component {

  state = {
     articles : [
      {
        id: uuid.v4(),
        isexpanded: false,
        logo: 'logo',
        color: '#16a085',
        title: 'The Practical Dev',
        articletitle: 'Learning React. Start Small',
        author: '@dceddia',
        date: new Date().toDateString(),
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sollicitudin aliquet sagittis. Aenean faucibus sem tortor, nec porttitor dui faucibus quis. Aenean gravida non massa tincidunt tincidunt.',
        textcolor: '#000',
        picture: '',
        picauthor: '@dceddia',
        stats: [125, 10, 70, 202]
      }, {
        id: uuid.v4(),
        isexpanded: true,
        logo: 'logo2',
        color: '#e67e22',
        title: 'Programming. React',
        articletitle: 'Become advanced developer',
        author: '@ihormushtruk',
        date: new Date().toDateString(),
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sollicitudin aliquet sagittis. Aenean faucibus sem tortor, nec porttitor dui faucibus quis. Aenean gravida non massa tincidunt tincidunt.',
        textcolor: '#000',
        picture: '',
        picauthor: '@ihormushtruk',
        stats: [14, 20, 17, 30]
      }, {
        id: uuid.v4(),
        isexpanded: false,
        logo: 'logo2',
        color: '#8e44ad',
        title: 'Have a nice day. Health',
        articletitle: 'Start eating this and..',
        author: '@vladark',
        date: new Date().toDateString(),
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sollicitudin aliquet sagittis. Aenean faucibus sem tortor, nec porttitor dui faucibus quis. Aenean gravida non massa tincidunt tincidunt.',
        textcolor: '#000',
        picture: '',
        picauthor: '@vladark',
        stats: [2, 5, 7, 15]
      }, {
        id: uuid.v4(),
        isexpanded: true,
        logo: 'logo',
        color: '#c0392b',
        title: 'Elon musk((',
        articletitle: 'Race to space cake',
        author: '@huilon',
        date: new Date().toDateString(),
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sollicitudin aliquet sagittis. Aenean faucibus sem tortor, nec porttitor dui faucibus quis. Aenean gravida non massa tincidunt tincidunt.',
        textcolor: '#fff',
        picture: '',
        picauthor: '@huilon',
        stats: [0, 15, 8, 6]
      }
    ],
  }




  handleExpandOpen = (index, flag) => {
    const artDupl = this.state.articles.slice();
    const articleIndex = artDupl.findIndex((a) => a.id === index);
    artDupl[articleIndex].isexpanded = !flag;

    this.setState({
      articles: artDupl
    })
    // console.log(this.state.articles[0].isexpanded);
    // console.log(this.baseState.articles[0].isexpanded);
    // console.log(data[0].isexpanded);
  }


  handleExpandClose = (index, flag) => {
    // const clonnedArticles = this.state.articles.slice();
    // const articleIndex = clonnedArticles.findIndex((a) => a.id === index);
    // clonnedArticles[articleIndex].isexpanded = !flag;
    //
    // this.setState({
    //   articles: clonnedArticles,
    // })
    this.setState(prevState => ({
    articles: prevState.articles.map(
    obj => (obj.id === index ? Object.assign(obj, { isexpanded: !flag }) : obj)
  )
}));

}

  render() {
    const articles = this.state.articles.map((a, index) => {
      if (!a.isexpanded) {
        return (<CardMini
          article={a}
          key={index}
          handleExpandOpen={this.handleExpandOpen}
        />)
      }else{
        return (<CardExpanded
          article={a}
          key={index}
          handleExpandClose={this.handleExpandClose}
        />);
      }
    })
    return (<div className="back-gr">
      <div className='card-expand-holder'>
        {articles}
      </div>
    </div>);
  }
}

export default CardExpandHolder;
