import './css/CardMini.css';
import React, { Component } from 'react';
import logo from './img/logo.png';
import logo2 from './img/logo2.png';

class CardMini extends Component {

  handleCardExpand = (id,flag) => {
    this.props.handleExpandOpen(id,flag);
  };


  render(){
    const color = {
      backgroundColor: this.props.article.color,
      color: this.props.article.textcolor,
    };

    const articleObj = this.props.article;

    return(
      <div className='card-mini' style={color}>
        <div className='card-mini-header'>
          <div className='card-mini-header-logo'>
            <img src={articleObj.logo === 'logo' ? logo : logo2} alt='logo'/>
          </div>
          <div className='card-mini-header-title'>
            <div className='subject'><h1>{articleObj.title}</h1><span>{articleObj.date}</span></div>
            <div className='theme'><h1>{articleObj.articletitle}</h1></div>
            <div className='author'><h1>{articleObj.author}</h1></div>
          </div>
          <div className='card-mini-header-arrow'>
            <div><i
              className="fa fa-arrow-down"
              onClick={() => this.handleCardExpand(articleObj.id, articleObj.isexpanded)}
              ></i></div>
          </div>
        </div>

      </div>
    );
  }
}
export default CardMini;
